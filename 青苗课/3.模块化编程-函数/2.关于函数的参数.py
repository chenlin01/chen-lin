# 关于函数的参数

# 带有参数的函数,如何定义

# 在定义函数时,在小括号内可以定义行参(形式上的参数)
# def love(w):
#     print(f'i love you {w}')

# 调用带有行参的函数时,需要传递参数(实参)
# love('琛哥')

# love() # TypeError: love() missing 1 required positional argument: 'w'

# 带有多个参数的函数
def loves(x,w):
    print(f'{x}love {w}')

loves('晨林 ','琛哥')# 函数定义了几个参数,调用时就必须指定按顺序进行参数的传递