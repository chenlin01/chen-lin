# 用类方法装饰函数

class Outer():
    def newinner(func):
        Outer.func = func  # 把传递进来的函数定义为类方法
        return Outer.inner # 同时返回一个新的类方法

    def inner():
        print('拿到妹子微信')
        Outer.func()
        print('看一场午夜电影')


@Outer.newinner  # Outer.newinner(love) ==> Outer.inner
def love():
    print('和妹子谈谈人生喝喝茶。。。')
love()          # love()  ==> Outer.inner()


