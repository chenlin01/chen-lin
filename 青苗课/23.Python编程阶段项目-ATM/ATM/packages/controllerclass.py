import random,pickle,os
from packages import cardclass as card
from packages import personclass as person

# 操作控制类
class Controller():
    # 数据存储格式
    userid_cardid_dict = {} # {身份证ID:银行卡ID}
    cardid_userobj_dict = {} # {银行卡ID:用户对象}

    # 数据存储的url
    user_file_url = './databases/user.txt'
    card_file_url = './databases/userid.txt'

    def __init__(self):
        # 加载所有数据信息
        self.__loaddata()

    def __loaddata(self):
        # 检测文件是否存在
        if os.path.exists(self.card_file_url):
            # 读取数据
            with open(self.card_file_url,'rb') as fp:
                self.userid_cardid_dict = pickle.load(fp)

        if os.path.exists(self.user_file_url):
            # 读取数据
            with open(self.user_file_url, 'rb') as fp:
                self.cardid_userobj_dict = pickle.load(fp)

    # 1.注册
    def register(self):
        # 获取用户输入的  用户名，身份证号，手机号，密码
        name = self.__getusername()
        userid = self.__getuserid()
        # 检测 身份证号 是否已经存在
        if userid in self.userid_cardid_dict:
            print(f'当前用户已经存在，卡号为：{self.userid_cardid_dict[userid]}')
            return
        phone = self.__getuserphone()
        password = self.__getuserpwd()

        # 创建一个银行卡
        cardid = random.randint(100000,999999)
        cardobj = card.Card(cardid,password)

        # 创建用户对象。和银行卡进行绑定
        userobj = person.Person(name,userid,phone,cardobj)

        # 创建需要保存的数据格式
        #  userid_cardid_dict {身份证ID:银行卡ID}
        #  cardid_userobj_dict {银行卡ID:用户对象}
        self.userid_cardid_dict[userid] = cardid
        self.cardid_userobj_dict[cardid] = userobj

        # 完成创建
        print(f'恭喜{name}开户成功,卡号为：{cardid}, 余额为：{cardobj.money}元')

    # 2.查询
    def query(self):

        # 获取用户输入的卡号
        cardid = int(input('请输入您的卡号：'))

        # 验证卡号是否存在，
        if cardid not in self.cardid_userobj_dict:
            print('当前卡号不存在')
            return
        # 获取卡对象
        cardobj = self.cardid_userobj_dict[cardid].card
        # 存在，输入密码
        if self.__checkpwd(cardobj):
            # 验证卡是否锁定
            if cardobj.islock:
                print('当前卡已经锁定，请先解卡')
                return
            else:
                # 通过卡号获取当前卡的余额信息
                print(f'您当前卡号为:{cardid},余额为:{cardobj.money}元')

    # 3.存钱
    def add_monry(self):
        print('存钱功能')

    # 4.取钱
    def get_money(self):
        print('取钱功能')

    # 5.转账
    def save_money(self):
        print('转账功能')

    # 6.改密
    def change_pwd(self):
        print('改密功能')

    # 7.锁卡
    def lock(self):
        print('锁卡功能')

    # 8.解卡
    def unlock(self):
        print('解卡功能')

    # 9.补卡
    def new_card(self):
        print('补卡功能')

    # 0.退出
    def save(self):
        # 把当前数据 写入到文件中
        with open(self.card_file_url,'wb+') as fp:
            pickle.dump(self.userid_cardid_dict,fp)
        with open(self.user_file_url,'wb+') as fp:
            pickle.dump(self.cardid_userobj_dict,fp)
        print('欢迎下次登录')

    # 获取用户名的私有方法
    def __getusername(self):
        while True:
            name = input('请输入您的用户名：')
            if not name:
                print('您输入的用户名有误，请重新输入。。。')
                continue
            else:
                return name

    def __getuserid(self):
        while True:
            userid = input('请输入您的 身份证号 ：')
            if not userid:
                print('输入内容有误，请重新输入。')
                continue
            else:
                return userid

    def __getuserphone(self):
        while True:
            name = input('请输入您的 手机号 ：')
            if not name:
                print('输入内容有误，请重新输入。')
                continue
            else:
                return name

    def __getuserpwd(self):
        while True:
            pwd = input('请输入您的 密码 ：')
            if not pwd:
                print('输入内容有误，请重新输入。')
                continue
            else:
                # 密码正确后，输入确认密码
                repwd = input('请再次输入密码，进行确认：')
                if repwd == pwd:
                    return pwd
                else:
                    print('两次密码不一致，请重新输入密码')
                    continue


    # 检测密码是否正确
    def __checkpwd(self,cardobj):
        num = 3
        while True:
            # 获取密码
            pwd = input('请输入您的密码：')
            # 检测密码是否正确
            if pwd == cardobj.password:
                return True
            else:
                num -= 1
                if num == 0:
                    # 直接锁卡
                    cardobj.islock = True
                    print('您当前的卡已经被锁定')
                    break
                else:
                    print(f'密码错误，您还有{num}次机会')

