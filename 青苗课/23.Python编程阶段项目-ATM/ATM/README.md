# ATM

项目说明文档



### 项目基本功能

> 当前的ATM系统模拟实现银行ATM机的操作系统功能

1. 注册
2. 查询
3. 存钱
4. 取钱
5. 转账
6. 改密
7. 锁卡
8. 解卡
9. 补卡
10. 退出



### 项目基本结构

```python
ATM/        			# 项目目录
├── README.md     # 项目文档
├── main.py       # 程序单入口文件
├── packages/     # 包
│   ├── __init__.py
│   ├── cardclass.py         # 银行卡类
│   ├── controllerclass.py   # 操作控制类
│   ├── personclass.py       # 用户类
│   └── viewsclass.py        # 视图显示类
└── databases/     # 数据目录
		├── user.txt   
		└── userid.txt
```

### 运行环境

+ 系统： windows/linux/Mac
+ 版本：python3.5+
+ 其它：无



### 迭代计划

+ 增加银行操作日志



