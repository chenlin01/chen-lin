# 其他运算符

# in not in 检测成员是否存在
s = 'iloveyou'
a = ['love', 'i', 'you']
b = ('love', 'i', 'you')
c = {'love', 'i', 'you'}
d = {1:'love', 2:'i', 3:'you'} # 如果是字典类型,只能检测 键
# print('love' in s) # 检查 love 字符串 是否存在于 s 变量中
# print('love' in a)
# print('love' in b)
# print('love' in c)
# print('love' in d)
# print(2 in d)

# print('love' not in s) # 检查 love 字符串 是否 不存在 于 s 变量中
# print('love' not in a)
# print('love' not in b)
# print('love' not in c)
# print('love' not in d)
# print(2 not in d)

# id() 变量的 id属性获取,函数用于获取对象内存地址
a = 10
b = 10

# is is not 检测两个变量是否引用了同一个对象

# is 是判断两个标识符是不是引用自同一个对象
# print(id(a),id(b))
# print(a is b)

# is not 是判断两个标识符是不是引用自不同对象
print(a is not b)































