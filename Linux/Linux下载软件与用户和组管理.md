# 使用yum源管理软件的步骤
- 配置yum配置文件
- 清空yum源缓存
- 软件安装
- mount: 挂载
    - mount 源文件 挂载文件
- umount: 卸载
    - umount 源文件/挂载文件
- 实际在vm虚拟机中的操作
    - 虚拟机挂载光盘信息
    - 创建yum源路径                               mkdir /mnt/cdrom
    - 挂载光盘到郁闷路径下                          mount /dev/cdrom   /mnt/cdrom
    - 修改yum配置文件
        路径: /etc/yum.repos.d/
        文件: vim name.repo
        内容: 
            [name]                              yum标识
            name = name                         yum名称
            baseurl = file:///mnt/cdrom         yum路径
            gpgcheck = 0                        不校验yum软件包的签名信息
            enabled = 1                         开机自启
    - yum repolist all                          查看所有yum源状态信息
    - yum list all                              列出所有的yum软件包
    - yum clean all                             清除yum缓存
    - yum install packagename                   安装yum软件包
- 常用yum命令
    - yum remove packagename                    删除软件包
    - yum info packagename                      查看软件包详细信息
    - yum search packagename                    查找软件包
    - yum update packagename                    更新软件包
- yum安装软件包组
    - yum grouplist                             列出所有可用组
    - yum groupinfo                             查看组的信息
    - yum groupinstall                          安装软件包组
    - yum grouperase                            删除软件包组
    - yum groupupdate                           更新软件包组
- 使用yum安装本地rpm包
    - yum localinstall **.rpm                   安装本地rpm包
- 源码安装:
    - 把程序的源代码给你了,你执行就行,看README
        - 检查源代码所处的编译环境
        - 编译
        - 安装
    - example:
        - anzhuang vmware tools
        - NTFS-3g
    - RPM软件包安装:
        - 
        
            
