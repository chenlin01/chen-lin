⼀ Python语⾔言的前世今⽣生
Python的创始⼈是吉多·范罗苏姆(Guido van Rossum)。

1989年的圣诞节期间，吉多·范罗苏姆为了在阿姆斯特丹打发时间，决⼼开发⼀个新的脚本解释程序， 作为ABC语言的一种继承。

之所以选中Python作为程序的名字，是因为他是BBC电视剧——蒙提·派森的⻜行马戏团(Monty Python’s Flying Circus)的爱好者。

ABC是由吉多参加设计的一种教学语言。就吉多本⼈看来，ABC这种语⾔非常优美和强⼤，是专⻔为⾮ 专业程序员设计的。但是ABC语言并没有成功，究其原因，吉多认为是⾮开放造成的。

吉多决心在Python中避免这一错误，并获取了⾮常好的效果，完美结合了C和其他⼀些语⾔。

龟叔:

2005年加入谷歌至2012年，

2013年加入Dropbox直到现在，

依然掌握着Python发展的核⼼方向，被称为仁慈的独裁者。

一，Python的发展历史

1989年年，为了打发圣诞节假期，Guido开始写Python语⾔的编译器。
1991年，第⼀个Python编译器诞生。它是⽤用C语言实现的，并能够调⽤用C语⾔的库文件。
Python 1.0 - January 1994 增加了 lambda, map, filterand reduce.
Python 2.0 - October 16, 2000，加⼊了内存回收机制，构成了现在Python语言框架的基础
Python 2.4 - November 30, 2004, 同年⽬前最流行的WEB框架Django 诞⽣ 。。。
Python 2.7 - July 3, 2010 2014年年11⽉月，宣布Python2.7支持到2020年年，并重申不会发布2.8版本，因为预计⽤户将尽快转向 Python3.4+
Python 3.0 - December 3, 2008 。。。
Python 3.4 - March 16, 2014
Python 3.5 - September 13, 2015
Python 3.6 - December 16,2016
Python 3.7 - 2018年6⽉27日
Python 3.8 - 2019年10⽉15⽇
二，Python的优点

第⼀，Python的数据分析能力非常突出。

它经常被⽤于数据分析领域，甚⾄成了大数据的标配，就因为Python语⾔对数据的统计和分析的⽅便。随着时间的发展，这个软件已经是⼀一个⽣态了，在数据领域已经是⼀个杀⼿锏级的应⽤，⼈工智能也已经离不开python了。

第二，Python的语法⾮常的简单，可读性⾮常强。

配合Markdown语言，你甚至不需要怎么调整，就 能写出⼀篇不错的可用于发表的文章来。学习这门语言的要求基础几乎就是0。

第三，Python的学习资源⾮常丰富。

因为使⽤广泛，学习⽅便，所以相关的资源也是⾮常的多，⾮常适合新手来学习。

三，Python的主要应⽤用领域:
云计算:
云计算最火的语言， 典型应用OpenStack
WEB开发:
众多优秀的WEB框架，众多大型⽹站均为Python开发，Youtube, Dropbox, 豆瓣。。。
科学运算、⼈工智能:
典型库NumPy, SciPy, Matplotlib, Enthought librarys,pandas
系统运维:
运维⼈员必备语言
爬虫:
通过代码来模拟人进⾏⻚面访问,对信息进行批量的获取
金融:
量化交易，⾦融分析，在⾦融工程领域，Python不但在用，且用的最多，⽽且重要性逐年提⾼。
原因:作为动态语言的Python，语⾔结构清晰简单，库丰富，成熟稳定，科学计算和统计分析都很⽜逼，生产效率远远高于c,c++,java,尤其擅⻓策略回测
图形GUI: PyQT, WxPython,TkInter
**Python可以应用于众多领域，如:数据分析、组件集成、⽹络服务、图像处理、数值计算和科学计算 等众多领域。**⽬前业内⼏乎所有大中型互联⽹企业都在使用Python，如:Youtube、Dropbox、BT、 Quora(中国知乎)、⾖瓣、知乎、Google、Yahoo!、Facebook、NASA、百度、腾讯、汽⻋车之家、 美团等。

互联⽹公司⼴泛使用Python来做的事一般有:⾃动化运维、⾃动化测试、⼤数据分析、爬虫、 Web 等。