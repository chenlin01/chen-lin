# Linux精髓在于命令行
- Linux命令的通用命令格式
    - 命令字 [选项] [参数]
        - 选项:
            - 作用:用于调节命令的具体功能
                - "-"引导短格式选项(单个字符) EX:"-1"
                - "--"引导长格式选项(多个字符) EX:"--color"
                - 多个端格式选项可以写在一起
                    - -a -l -z -x  -->  -alzx
        - 参数:
            - 命令操作的对象,有文件,目录等
            
        - EX:ls -l /home (ls:所有文件;  -l:长格式;  /home:根目录下的home目录)
# Linux目录结构
- boot:存放系统引导文件和内核文件
- bin:存放可执行文件(二进制文件,普通用户命令)
- sbin:root用户执行命令
- home:普通用户的家目录
- root:root用户家目录
- dev:放置所有设备文件(外设)
- etc:放置所有配置文件(服务)
- lib/lib64:动态链接库文件(共享库) 类似于:dll
- media:媒体库文件
- opt:文件安装目录,安装软件就在opt目录下
- mnt:挂载点 mont * *
- var:存放一些需要改变数据的文件 日志,某些大文件的溢出区
- proc:虚拟文件系统目录,存放的是内存的一个映射
    - cat /proc/cpuinif: 查看CPU运行状态
    - cat /proc/meminfo: 查看内存运行状态
    - cat /proc/version: 查看系统版本
    - uname -m: 查看查看CPu架构
    - uname -r: 查看内核版本
- usr:最大的目录,只要用到的目录或者文件都在这
    - /usr/sbin
    - /usr/doclinx
- tmp:共享文件夹,临时目录
- lost+found:平时为空,只有系统在非正常关机时才会有,保存意外掉电中内存的数据
# Linux常用命令
- 目录操作命令:
    - ls:列出当前目录下的文件及目录
        - 格式: ls [选项] [目录或文件名]
        - 选项
            - -l: 以长格式显示
            - -a: 显示所有文件目录信息,包括隐藏文件
            - -d: 显示目录本身的属性
            - -h: 显示详细信息,变换了统计单位
            - --color: 以颜色区分不同类型的文件    
    - 在Linux中以.开头的文件是隐藏文件
        - ll == ls -l
                    
    - cd:切换工作目录
        - cd [目录位置]
        - 绝对路径: cd/home/chenlin
        - 相对路径: 
            - cd ..:返回到上级目录
            - cd -:回到上一次所在的工作目录
            - cd ../../:返回上一级目录的上一级目录
            - cd ~:返回到用户的home目录   
                [root@localhost Desktop]# cd ~/Documents/
                [root@localhost Documents]# pwd
                /root/Documents
                [root@localhost Documents]# 
        - cd cd. cd..
            - 针对目录来说: . 代表当前位置/目录  pwd
            - 针对文件来说: . 在Linux中代表隐藏文件
            - cd : 什么都不跟,代表切换到当前用户home目录
            - cd. : 当前目录
            - cd.. :代表上级目录
            - ~ : 用户home目录
            - - :上次操作的目录
    - pwd:查看当前所在的工作目录位置(告诉你自己在哪)
        - pwd
    - mkdir:创建一个新目录
        - 格式:mkdir [选项] [路径] 目录名
            - -p: 递归创建多级目录 
                [root@localhost ~]# mkdir -p b/c/d/e
                     
    - rmdir:删除空目录
             
        [root@localhost ~]# rmdir adir/
        [root@localhost ~]# ls
         anaconda-ks.cfg  bdir  Desktop  Documents  Downloads  initial-setup-ks.cfg  Music  Pictures  Public  Templates  Videos
    - -p:递归删除
            
        [root@localhost ~]# rmdir -p a/b/c/d/e/

- 文件操作命令:
    - touch:创建一个空文件,更新文件时间标记
        - 格式:touch 文件名
            [root@localhost ~]# touch aa.txt
            
    - echo:打印某样东西
         [root@localhost ~]# echo "hello world"
        hello world
    - echo "hello world" > a.txt:重定向写
        [root@localhost ~]# echo "对酒当歌,人生几何" > a.txt
        [root@localhost ~]# ls
        anaconda-ks.cfg  a.txt  Desktop  Documents  Downloads  initial-setup-ks.cfg  Music  Pictures  Public  Templates  Videos
        [root@localhost ~]# cat a.txt
        对酒当歌,人生几何

    - cp:复制文件或者目录
        - 格式:cp [选项] 源文件/目录 目标文件/目录
            [root@localhost bb]# cp /root/aa/a.txt /root/bb/
        - -r: 递归复制整个目录树
            [root@localhost bb]# cp -r /root/aa/ /root/bb/
            [root@localhost bb]# ls
            aa  a.txt

    - mv: 移动文件或者目录
        - 格式:mv [选项] 源文件/目录  目标文件/目录
        - 移动文件:
            [root@localhost text]# ls
            a.txt  b.txt
            [root@localhost text]# mv b.txt /root/text-2/
            [root@localhost text]# ls
            a.txt
            [root@localhost text-2]# ls
             a.txt  b.txt
        - 移动目录:
            [root@localhost ~]# mkdir aa
            [root@localhost ~]# ls
            aa               Documents             Music     Templates  Videos
            anaconda-ks.cfg  Downloads             Pictures  text
            Desktop          initial-setup-ks.cfg  Public    text-2
            [root@localhost ~]# mv aa/ text-2/
            [root@localhost ~]# cd text-2/
            [root@localhost text-2]# ls
            aa  a.txt  b.txt
        - 移动重命名:(若移动的目标位置与原位置相同则此操作相当于重命名)
                [root@localhost ~]# ls
                anaconda-ks.cfg  Documents  initial-setup-ks.cfg  Pictures  Templates  text-2
                Desktop          Downloads  Music                 Public    text       Videos
                [root@localhost ~]# mv text/ text-1/
                [root@localhost ~]# ls
                anaconda-ks.cfg  Documents  initial-setup-ks.cfg  Pictures  Templates  text-2
                Desktop          Downloads  Music                 Public    text-1     Videos

    - rm:删除文件或目录
        - 格式:rm [选项] 文件/目录
            - rm: 删除某个文件
                    [root@localhost ~]# ls
                    aa  aa.txt  anaconda-ks.cfg  bb  Desktop  Documents  Downloads  initial-setup-ks.cfg  Music  Pictures  Public  Templates  Videos
                    [root@localhost ~]# rm aa.txt
                    rm: remove regular empty file ‘aa.txt’? y
                    [root@localhost ~]# ls
                    aa  anaconda-ks.cfg  bb  Desktop  Documents  Downloads  initial-setup-ks.cfg  Music  Pictures  Public  Templates  Videos
            - rm -r: 递归删除整个目录树
                    [root@localhost ~]# ls
                    aa  anaconda-ks.cfg  bb  Desktop  Documents  Downloads  initial-setup-ks.cfg  Music  Pictures  Public  Templates  Videos
                    [root@localhost ~]# rm -r bb
                    rm: descend into directory ‘bb’? y
                    rm: remove regular file ‘bb/a.txt’? y
                    rm: descend into directory ‘bb/aa’? y
                    rm: remove regular empty file ‘bb/aa/aa.txt’? y
                    rm: remove regular file ‘bb/aa/a.txt’? y
                    rm: remove directory ‘bb/aa’? y
                    rm: remove directory ‘bb’? y
                    [root@localhost ~]# ls
                    aa  anaconda-ks.cfg  Desktop  Documents  Downloads  initial-setup-ks.cfg  Music  Pictures  Public  Templates  Videos
            - rm -rf: 强制删除不给任何提示
                    [root@localhost ~]# ls
                    a  anaconda-ks.cfg  Desktop  Documents  Downloads  initial-setup-ks.cfg  Music  Pictures  Public  Templates  Videos
                    [root@localhost ~]# rm -rf a
                    [root@localhost ~]# ls
                    anaconda-ks.cfg  Desktop  Documents  Downloads  initial-setup-ks.cfg  Music  Pictures  Public  Templates  Videos
            - rm -rf *:删除目录下所有文件和目录
    - find:用于查找文件或者目录
        - 格式:find [查找范围] [查找条件]
            - 查找条件:
                - -name: 按文件名查找
                        [root@localhost ~]# ls
                        anaconda-ks.cfg  Documents  initial-setup-ks.cfg  Pictures  Templates  text-2
                        Desktop          Downloads  Music                 Public    text-1     Videos
                        [root@localhost ~]# find / -name initial-setup-ks.cfg
                        /root/initial-setup-ks.cfg
                        [root@localhost ~]# find / -name initial-*
                        /root/initial-setup-ks.cfg

                - -type: 按文件类型查找
                - f: 普通文件
                        [root@localhost ~]# find / -name init* -type f
                        /root/initial-setup-ks.cfg
                        - d: 目录(不支持通配符)
                        [root@localhost ~]# find / -name text -type d
                        /usr/lib/python2.7/site-packages/kitchen/text
                        /usr/share/icons/hicolor/16x16/stock/text
                        /usr/share/icons/hicolor/22x22/stock/text
                        /usr/share/icons/hicolor/24x24/stock/text
                        /usr/share/icons/hicolor/256x256/stock/text
                        /usr/share/icons/hicolor/32x32/stock/text
                        /usr/share/icons/hicolor/36x36/stock/text
                        /usr/share/icons/hicolor/48x48/stock/text
                        /usr/share/icons/hicolor/96x96/stock/text
                        /usr/share/icons/hicolor/scalable/stock/text
                        /usr/share/icons/hicolor/128x128/stock/text
                        /usr/share/icons/hicolor/192x192/stock/text
                        /usr/share/icons/hicolor/64x64/stock/text
                        /usr/share/icons/hicolor/72x72/stock/text
                        /usr/share/mime/text
                        /usr/share/plymouth/themes/text
                        [root@localhost ~]# 

                - b: 块设备文件
                        
                - c: 字符设备文件
            - user: 按文件属主查找(这个文件是谁的)
                        [root@localhost ~]# find / -user root -name initial-setup-ks.cfg
                        /root/initial-setup-ks.cfg
                        
                        [root@localhost ~]# find / -user chenlin -type f -name cl.initial-setup-ks.cfg
                        /home/chenlin/cl.initial-setup-ks.cfg
            - size: 按文件大小查找
                        [root@localhost ~]# find /var/ -size +100k -type f
                        /var/lib/yum/history/history-2020-10-28.sqlite
                        /var/lib/rpm/Packages
                        /var/lib/rpm/Basenames
                        /var/lib/rpm/Requirename
                        /var/lib/rpm/Providename
                        /var/lib/rpm/Dirnames
                        /var/lib/rpm/Sha1header
                        /var/lib/rpm/__db.001
                        /var/lib/rpm/__db.003
                        /var/lib/mlocate/mlocate.db
                        /var/log/lastlog
                        /var/log/audit/audit.log
                        /var/log/messages
                        /var/log/anaconda/syslog
                        /var/log/anaconda/anaconda.packaging.log
                        /var/log/anaconda/anaconda.storage.log
                        /var/log/dmesg.old
                        /var/log/dmesg
                        /var/cache/man/index.db
                        /var/cache/ibus/bus/registry
                - +:大于多少K
                - -:小于多少k
                        [root@localhost ~]# find /var/ -size +100k -size -110k -type f
                        /var/cache/ibus/bus/registry     
                - -a: 逻辑与
                - -o: 逻辑或
                        [root@localhost ~]# find / -name anaconda-ks.cfg -o -name initial-setup-ks.cfg
                        /root/anaconda-ks.cfg
                        /root/initial-setup-ks.cfg

                - ! :逻辑非
                        [root@localhost ~]# find / ! -name temp -type f

- 文件内容操作命令:
    - cat: 文件内容查看,显示出文件的全部内容
        - 格式: cat [文件名]
                [root@localhost ~]# cat text
                对酒当歌,人生几何
    - less(more):全屏方式分页显示文件内容
         - less/more [文件名]
                [root@localhost ~]# less text
            - 操作方法:
                - 回车键: 逐行滚动
                - 空格键: 向下翻页
                - b键: 向上翻页
                - q键: 退出
            - more用法与less一致,这是多了一个百分比显示        
    - head(头部):显示文件头部若干行,默认十行
        - 格式:head [选项] 文件名
                [root@localhost ~]# head initial-setup-ks.cfg
                #version=RHEL7
                # X Window System configuration information
                xconfig  --startxonboot
                
                # License agreement
                eula --agreed
                # System authorization information
                auth --enableshadow --passalgo=sha512
                # Use CDROM installation media
                cdrom
        - 查看头部一行:
                [root@localhost ~]# head -n +1 initial-setup-ks.cfg 
                #version=RHEL7
    - tail(尾部):显示文件尾部若干行,默认十行
        - 格式:tail [选项] 文件名
                [root@localhost ~]# tail initial-setup-ks.cfg
                @guest-agents
                @guest-desktop-agents
                @input-methods
                @internet-browser
                @multimedia
                @print-client
                @x11
                
                %end
        - 查看尾部后5行
                [root@localhost ~]# tail -n -5 initial-setup-ks.cfg 
                @print-client
                @x11
                
                %end
        - 注意符号的使用 + - 代表的意思不同
    - wc: 统计文件内容的,默认情况下统计结果为: 行数 词数 字节数
        - 格式: wc [选项] 文件名
                [root@localhost ~]# wc initial-setup-ks.cfg 
                49  110 1269 initial-setup-ks.cfg
        - 选项
            - -w: 统计字数, 一个字被定义为由空格,条格,换行 以这些为分隔字符串
                    [root@localhost ~]# wc -w initial-setup-ks.cfg 
                    110 initial-setup-ks.cfg
  
            - -l: 行数
                    [root@localhost ~]# wc -l initial-setup-ks.cfg 
                    49 initial-setup-ks.cfg
            - -c: 字节数
                    [root@localhost ~]# wc -c initial-setup-ks.cfg 
                    1269 initial-setup-ks.cfg
            - -m: 字符数 -m -c不能一起使用
                    [root@localhost ~]# wc -m initial-setup-ks.cfg 
                    1269 initial-setup-ks.cfg
            - -L: 打印最长行的长度
                    [root@localhost ~]# wc -L initial-setup-ks.cfg 
                    167 initial-setup-ks.cfg
                    
                    [root@localhost ~]# wc -l -c -w -L initial-setup-ks.cfg 
                    49  110 1269  167 initial-setup-ks.cfg

    - grep: 在文件中查找并显示包含指定字符串的行
        - 格式:grep [选项] 查找条件 目标文件
                [root@localhost ~]# grep 'boot' initial-setup-ks.cfg 
                xconfig  --startxonboot
                # Run the Setup Agent on first boot
                firstboot --enable
                network  --bootproto=dhcp --device=eno16777728 --onboot=off --ipv6=auto
                network  --bootproto=dhcp --hostname=localhost.localdomain
                # System bootloader configuration
                bootloader --location=mbr --boot-drive=sda
        - 选项:
            - -c: 显示匹配行的数量
                    [root@localhost ~]# grep -c 'boot' initial-setup-ks.cfg 
                    7  
            - -i: 查找时不区分大小写
                    [root@localhost ~]# grep -i 'agent' initial-setup-ks.cfg 
                    # Run the Setup Agent on first boot
                    @guest-agents
                    @guest-desktop-agents
            - -v: 翻转查找 
                    [root@localhost ~]# grep -c -v 'boot' initial-setup-ks.cfg 
                    42  
            - 查找条件设置:
                - 查找条件用引号引起来
                - "^...."表示以什么开头
                        [root@localhost ~]# grep '^#' anaconda-ks.cfg 
                        #version=RHEL7
                        # System authorization information
                        # Use CDROM installation media
                        # Run the Setup Agent on first boot
                        # Keyboard layouts
                        # System language
                        # Network information
                        # Root password
                        # System timezone
                        # X Window System configuration information
                        # System bootloader configuration
                        # Partition clearing information

                - "....$"表示以什么结尾
                        [root@localhost ~]# grep "ation$" anaconda-ks.cfg 
                        # System authorization information
                        # Network information
                        # X Window System configuration information
                        # System bootloader configuration
                        # Partition clearing information

                - "^$"匹配空行  
                        [root@localhost ~]# grep -c '^$' anaconda-ks.cfg 
                        5
- 归档及压缩命令:
    - tar: 压缩与解压缩,制作归档文件和释放归档文件
        - 格式:
        - 制作归档文件:
            - tar [选项] [目录]/归档文件名 源文件或目录
        - 解压归档文件:
            - tar [选项] 归档文件 [-C 目标文件]
        - 命令:
            - 压缩
                - tar -czvf [存放路径]归档文件名.tar.gz 源文件或目录
                - 案例1:
                            [root@localhost ~]# ls
                            anaconda-ks.cfg  Downloads             Pictures  Templates  text-2
                            Desktop          initial-setup-ks.cfg  Public    text       Videos
                            Documents        Music                 temp      text-1
                            [root@localhost ~]# tar -czvf text.tar.gz text
                            text
                            [root@localhost ~]# ls
                            anaconda-ks.cfg  initial-setup-ks.cfg  temp       text-2
                            Desktop          Music                 Templates  text.tar.gz
                            Documents        Pictures              text       Videos
                            Downloads        Public                text-1
                - 案例2:
                            [root@localhost ~]# tar -czvf /root/var.tar.gz /var/
                            [root@localhost ~]# ls
                            anaconda-ks.cfg       Music      text          var.tar.gz
                            Desktop               Pictures   text-1        Videos
                            Documents             Public     text-2
                            Downloads             temp       text.tar.bz2
                            initial-setup-ks.cfg  Templates  text.tar.gz
                - 案例3:
                            [root@localhost ~]# tar -czvf a.tar.gz text-1 text-2
                            text-1/
                            text-1/a.txt
                            text-2/
                            text-2/a.txt
                            text-2/b.txt
                            text-2/aa/
                            [root@localhost ~]# ls
                            anaconda-ks.cfg  initial-setup-ks.cfg  Templates     text.tar.gz
                            a.tar.gz         Music                 text          var.tar.bz2
                            Desktop          Pictures              text-1        var.tar.gz
                            Documents        Public                text-2        Videos
                            Downloads        temp                  text.tar.bz2

            - tar -cjvf [存放路径]归档文件名.tar.bz2 源文件或目录
                - 案例1:
                            [root@localhost ~]# tar -cjvf text.tar.bz2 text
                            text
                            [root@localhost ~]# ls
                            anaconda-ks.cfg  initial-setup-ks.cfg  temp       text-2
                            Desktop          Music                 Templates  text.tar.bz2
                            Documents        Pictures              text       text.tar.gz
                            Downloads        Public                text-1     Videos
                - 案例2:
                            [root@localhost ~]# tar -cjvf /root/var.tar.bz2 /var/
                            [root@localhost ~]# ls
                            anaconda-ks.cfg       Music      text          var.tar.bz2
                            Desktop               Pictures   text-1        var.tar.gz
                            Documents             Public     text-2        Videos
                            Downloads             temp       text.tar.bz2
                            initial-setup-ks.cfg  Templates  text.tar.gz
                - 案例3:
                            [root@localhost ~]# tar -cjvf a.tar.bz2 text-1 text-2
                            text-1/
                            text-1/a.txt
                            text-2/
                            text-2/a.txt
                            text-2/b.txt
                            text-2/aa/
                            [root@localhost ~]# ls
                            anaconda-ks.cfg  Downloads             temp       text.tar.bz2
                            a.tar.bz2        initial-setup-ks.cfg  Templates  text.tar.gz
                            a.tar.gz         Music                 text       var.tar.bz2
                            Desktop          Pictures              text-1     var.tar.gz
                            Documents        Public                text-2     Videos
            - -c: 压缩
            - -z: 压缩为.gz格式
            - vf: 显示详细信息
        - 解压缩
            - tar -xzvf [存放路径]归档文件名.tar.gz [-C 解压目录]
                - 案例1:后面什么都不跟,解压到当前路径
                            [root@localhost ~]# tar -xzvf var.tar.gz 
                            [root@localhost ~]# ls
                            anaconda-ks.cfg  initial-setup-ks.cfg  text          var.tar.bz2
                            a.tar.bz2        Music                 text-1        var.tar.gz
                            a.tar.gz         Pictures              text-2        Videos
                            Desktop          Public                text.tar.bz2
                            Documents        temp                  text.tar.gz
                            Downloads        Templates             var
                        
            - tar -xjvf [存放路径]归档文件名.tar.bz2 [-C 解压目录] 
                - 案例1:解压到指定目录
                            tar -xjvf var.tar.bz2 -C text-1 
                            [root@localhost text-1]# ls
                            a.txt  var
            - -x:解压
    - ln: link 链接
        - 链接有两种
        - 软链接: ln -s 源文件 目标文件
                [root@localhost ~]# ln -s a.txt a.txt.soft
                [root@localhost ~]# ll
                total 34596
                -rw-------.  1 root root     1218 Oct 28 07:09 anaconda-ks.cfg
                -rw-r--r--.  1 root root      202 Oct 28 11:07 a.tar.bz2
                -rw-r--r--.  1 root root      200 Oct 28 11:06 a.tar.gz
                -rw-r--r--.  1 root root       13 Oct 28 21:38 a.txt
                lrwxrwxrwx.  1 root root        5 Oct 28 21:46 a.txt.soft -> a.txt
                drwxr-xr-x.  2 root root        6 Oct 27 23:18 Desktop
                drwxr-xr-x.  2 root root        6 Oct 27 23:18 Documents
                drwxr-xr-x.  2 root root        6 Oct 27 23:18 Downloads
                -rw-r--r--.  1 root root     1269 Oct 27 23:16 initial-setup-ks.cfg
                drwxr-xr-x.  2 root root        6 Oct 27 23:18 Music
                drwxr-xr-x.  2 root root        6 Oct 27 23:18 Pictures
                drwxr-xr-x.  2 root root        6 Oct 27 23:18 Public
                -rw-r--r--.  1 root root        0 Oct 28 05:21 temp
                drwxr-xr-x.  2 root root        6 Oct 27 23:18 Templates
                -rw-r--r--.  1 root root       26 Oct 28 05:28 text
                drwxr-xr-x.  3 root root       28 Oct 28 11:30 text-1
                drwxr-xr-x.  3 root root       39 Oct 28 04:26 text-2
                -rw-r--r--.  1 root root      158 Oct 28 10:39 text.tar.bz2
                -rw-r--r--.  1 root root      155 Oct 28 10:37 text.tar.gz
                drwxr-xr-x. 22 root root     4096 Oct 28 10:05 var
                -rw-r--r--.  1 root root 15564158 Oct 28 11:02 var.tar.bz2
                -rw-r--r--.  1 root root 19824390 Oct 28 10:57 var.tar.gz
                drwxr-xr-x.  2 root root        6 Oct 27 23:18 Videos
        - 硬链接: ln 源文件 目标文件
            - 硬链接相当于cp -p +同步更新
                    [root@localhost ~]# gedit a.txt
                    [root@localhost ~]# cat a.txt
                    对酒当歌
                    meiyouzhongwen a 
                    [root@localhost ~]# cat a.txt.hard
                    对酒当歌
                    meiyouzhongwen a 
                    [root@localhost ~]#                   
    - alias :别名,为使用频率较高的命令设置简短的调用名称
        - 查看当前用户所有别名
            - alias [别名]
        - 设置别名
            - alias 别名="实际执行的命令"
                    [root@localhost ~]# alias aa="grep -c 'boot' initial-setup-ks.cfg"
                    [root@localhost ~]# alias aa
                    alias aa='grep -c '\''boot'\'' initial-setup-ks.cfg'
                    [root@localhost ~]# aa
                    7
        - 取消别名
                    [root@localhost ~]# unalias aa
                    [root@localhost ~]# aa
                    bash: aa: command not found...
                    [root@localhost ~]# 
                    
            - -a: 取消所有别名
        - 让别名永久生效:
            - 将别名设置在~/.bashrc文件中
                    [root@localhost ~]# gedit .bashrc
                    """
                    # .bashrc

                    # User specific aliases and functions
                    
                    alias rm='rm -i'
                    alias cp='cp -i'
                    alias mv='mv -i'
                    alias aa="grep -c 'boot' initial-setup-ks.cfg"
                    """
    - 在Linux中,不以后缀区分文件
- Linux中如何获得命令帮助:
    - help:内部命令查找(56条)
        - cd
        - kill
        - EX:help cd
    - --help:适用于大多数外部命令查找
        - EX:ls --help
    - 使用man手册进行命令查看(man:命令阅读手册) 
        - man  命令字
            - 上下键滚动文本
            - page down & page up 上下翻页
            - 空格键也支持翻页,回车键走一行
            - 输入/ ,可以查找
        - info也可以进行命令帮助查找(使用与man接近)
        - pinfo 命令名称:以浏览器的形式查看详细的GUN信息
        - /usr/share/doc:所有已安装软件的说明文件 
            - firefox file:///usr/share/doc
             















            