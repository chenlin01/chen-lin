
#银行卡类
class Card():
    # 卡号，密码，余额，是否锁卡
    def __init__(self,cardid,pwd,money=10,islock=False):
        self.card_id = cardid  # 卡号
        self.password = pwd    # 密码
        self.money = money     # 余额
        self.islock = islock   # 是否锁卡，False未锁卡，True锁卡
