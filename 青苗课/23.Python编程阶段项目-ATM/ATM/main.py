
from packages.viewsclass import Views
from packages.controllerclass import Controller

# 821001

class Main():

    def __init__(self):
        # 实例化视图对象
        view = Views()
        # 实例化操作控制类
        obj = Controller()

        while True:
            # 让用户选择操作
            num = input('用输入您要选择的操作：')
            # 需要验证用户的输入是否正确
            code = ['1','2','3','4','5','6','7','8','9','0']
            if num not in code:
                print('您输入的内容有误,请重新输入。')
                view.showfunc()
                # 跳过本次循环
                continue
            # 以下是使用流程控制。
            if num == '1':
                obj.register()  # 注册 开卡
            elif num == '2':
                obj.query()     # 查询 余额
            elif num == '3':
                obj.add_money() # 存款
            elif num == '4':
                obj.get_monry() # 取款
            elif num == '5':
                obj.save_money() # 转账
            elif num == '6':
                obj.change_pwd() # 改密
            elif num == '7':
                obj.lock()       # 锁卡
            elif num == '8':
                obj.unlock()     # 解卡
            elif num == '9':
                obj.new_pwd()    # 补卡
            elif num == '0':
                obj.save()       # 退出 保存
                break
            # 也可以改成其它形式
            # 例如字典
            # vardict = {'1': obj.register}  vardict[num]()

if __name__ == '__main__':
    Main()