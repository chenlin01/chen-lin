# 描述符的应用案例解析


'''
# 要求：学员的分数只能在0-100范围中
解决方法：
    1。在__init__方法中检测当前分数范围
         # 检测分数范围
        if score >= 0 and score <= 100:
            self.score = score
        这个解决方案只能在对象初始化时有效。
    2。 定义一个setattr魔术方法检测
        检测如果给score分数进行赋值时，进行分数的检测判断
            def __setattr__(self, key, value):
        # 检测是否是给score进行赋值操作
        if key == 'score':
            # 检测分数范围
            if value >= 0 and value <= 100:
                object.__setattr__(self, key, value)
            else:
                print('当前分数不符合要求')
        else:
            object.__setattr__(self,key,value)

        假如 学员的分数不止一个时怎么办，比如 语文分数，数学分数，英语分数
        另外就是当前这个类中的代码是否就比较多了呢？

    3。可以思考使用描述符来代理我们的分数这个属性
        1.定义Score描述符类
        2.把学生类中的score这个成员交给描述符类进行代理
        3.只要在代理的描述符类中对分数进行赋值和获取就ok了


'''
# 定义一个学生类，需要记录 学员的id，名字，分数
# class Student():
#     def __init__(self,id,name,score):
#         self.id = id
#         self.name = name
#         # self.score = score
#
#         # 检测分数范围
#         if score >= 0 and score <= 100:
#             self.score = score
#         else:
#             print('当前分数不符合要求')
#
#     def returnMe(self):
#         info =  f'''
#         学员编号:{self.id}
#         学员姓名:{self.name}
#         学员分数:{self.score}
#         '''
#         print(info)
#
#
#     def __setattr__(self, key, value):
#         # 检测是否是给score进行赋值操作
#         if key == 'score':
#             # 检测分数范围
#             if value >= 0 and value <= 100:
#                 object.__setattr__(self, key, value)
#             else:
#                 print('当前分数不符合要求')
#         else:
#             object.__setattr__(self,key,value)


#定义描述符类 代理分数的管理
class Score():
    def __get__(self, instance, owner):
        return self.__score
    def __set__(self, instance, value):
        if value >= 0 and value <= 100:
            self.__score = value
        else:
            print('分数不符合要求')

# 使用描述符类代理score分数属性
class Student():
    score = Score()
    def __init__(self,id,name,score):
        self.id = id
        self.name = name
        self.score = score

    def returnMe(self):
        info =  f'''
        学员编号:{self.id}
        学员姓名:{self.name}
        学员分数:{self.score}
        '''
        print(info)



# 实例化对象
zs = Student(1011,'张三疯',99)
zs.returnMe()
zs.score = -20
zs.score = 88
zs.returnMe()

