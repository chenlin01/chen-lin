# 面向对象 常用函数

class Demo():
    pass

class A():
    pass
class B(A):
    pass
class C(A):
    ccc = 'abc'

class D(B,C):
    name = 'a'
    __age = 20

    def say(self):
        pass


d = D()


# 检测类和对象相关
# issubclass(子类,父类) # 检测一个类是否为另一个类的子类
# res = issubclass(D,B)

# isinstance(对象,类) # 检测一个对象是否是该类或该类的子类的实例化结果
# res = isinstance(d,A)

# 操作类和对象成员相关
# hasattr(对象/类,'成员名称')  检测类/对象是否包含指定名称的成员
# res = hasattr(d,'name')

#getattr(对象/类,'成员名称') # 获取类/对象的成员的值
# res = getattr(d,'say')

# setattr(对象/类,'成员名称','成员的值') 设置类/对象的成员的属性值
res = setattr(d,'name','ooo')
# print(d.name)

#delattr(类/对象,'成员名称') 删除类/对象的成员属性 和 del 直接删除对象的成员是一样的结果
# delattr(D,'name')

# dir() #获取当前对象所以可以访问的成员的列表
res = dir(d)
print(res)


