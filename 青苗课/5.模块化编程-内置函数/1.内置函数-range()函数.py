# range() 函数

'''
range() 函数
功能: 能够生成一个指定的数字序列
参数:
    start: 开始的值 (如果该形参未提供默认值 0)
    stop: 结束的值
    [, step]: 可选,步进值 (如果该形参未提供默认值 1)
返回值: 可迭代的对象,数字序列
'''

# res = range(10)
# print(res)
# 转为列表数据
# print(list(res))

# 通过for循环 进行遍历
# for i in res:
#     print(i)

# 转为迭代器,使用next函数调用
# res = iter(res)
# print(next(res))
# print(next(res))

# 只写一个参数,就是从0开始到10之前
# res = range(10)

# 两个参数时,第一个参数时开始的值,第二个参数时结束从值(在结束值之前)
# res = range(5,10)

# 三个参数, 参数1 是开始值.  参数2 是结束值, 参数3 是步进值
# res = range(1,10,2)

# 获取一个倒叙的数字序列
# res = range(10,0,-1)

res = range(-10,-20,-1)
res = range(-20,-10)
res = range(-10,10)
print(list(res))





















