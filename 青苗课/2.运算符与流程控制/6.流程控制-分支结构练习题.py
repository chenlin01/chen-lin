# 分支结构练习题

# 注意:在书写分支结构时,一定要遵守python的语法要求

if True:
    pass # pass 在代码中 专门用于占位
    # print('aa') # 在相同区域代码块中的代码,一定要保持相同的缩进


'''
让用户输入四位数的年份,来计算当前这个年对应的 十二生肖,并输出结构
酉鸡,戌狗,亥猪,子鼠,丑牛,寅虎,卯兔,辰龙,巳蛇,午马,未羊,申猴
 1    2   3   4   5   6    7   8   9   10   11  12
获取用户输入 input(), print()
'''
# 获取用户输入
# year = int(input('请输入四位数的年份:'))
#
# if year % 12 == 1:
#     print('酉鸡')
# elif year % 12 == 2:
#     print('戌狗')
# elif year % 12 == 3:
#     print('亥猪')
# elif year % 12 == 4:
#     print('子鼠')
# elif year % 12 == 5:
#     print('丑牛')
# elif year % 12 == 6:
#     print('寅虎')
# elif year % 12 == 7:
#     print('卯兔')
# elif year % 12 == 8:
#     print('辰龙')
# elif year % 12 == 9:
#     print('巳蛇')
# elif year % 12 == 10:
#     print('午马')
# elif year % 12 == 11:
#     print('未羊')
# elif year % 12 == 12:
#     print('申猴')

# 定义12生肖字典

sx = {
    1:'酉鸡',
    2:'戌狗',
    3:'亥猪',
    4:'子鼠',
    5:'丑牛',
    6:'寅虎',
    7:'卯兔',
    8:'辰龙',
    9:'巳蛇',
    10:'午马',
    11:'未羊',
    12:'申猴'
 }

year = int(input('请输入四位数年份:'))
res = year%12
print(sx[res])