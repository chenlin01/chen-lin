# 图形界面应用
- 一概而过
# 网络配置
- 网络通信三种方法
    - 桥接
    - NAT
        - 只可以访问外网,外网无法访问虚拟机 
    - 仅本地模式
        - 仅本机跟虚拟机之间相互连接
- 通过NAT让虚拟机访问外网
- 通过仅本地模式让真机与虚拟机通信
# shell简介
- Linux精髓在于命令行操作
- 概念:
    - shell是一种特殊的程序
- 作用:
    - 内核与用户的一种接口
    - shell命令解释器
    - shell一种解释性的语言(内部命令/外部命令)
        - 内部命令:系统自带的,随系统内核一起启动(大概有56种) cd(改变路径)
        - 外部命令:就是一些额外的软件或者程序  ls(列出文件或者目录)
    - sh (Borurne Shell)     csh   ksh
    - BASH 是rhel种默认的一种
    - [root@localhost 桌面]#
        - root:代表当前登录用户
        - localhost:代表当前系统的主机名称
        - 桌面/Desktop:代表当前所在的位置
        - "#":代表当前登录用户是管理员用户
            - root:管理员
        - "$":代表当前登录用户为普通用户
            - [root@localhost ~]# su - chenlin
                - su - username 切换当前登录用户
                    - 有 - :代表切换bash环境
                    - 无 - :代表不切换bash环境
                - 由root用户到普通用户不需要密码
                - 由普通用户到root用户需要输入密码
                - exit:退出
        - 在Linux中严格区分大小写
        - 在Linux中一切皆文本
            - 查看系统当前主机名:hostname
            - 修改主机名并让其生效:
                                
                                hostnamectl set-hostname 主机名
                                [root@localhost ~]# hostname
                                chenlin
                                [root@localhost ~]# su -
                                上一次登录：四 10月 22 23:12:22 CST 2020pts/0 上
            - 关机命令:
                     
                     poweroff
                     init 0
                     shutdown -h now    现在关机
                     shutdown -h +15    15分钟后关机
                     halt
            - 重启命令:
            
                    reboot
                    shutdown -r now   现在重启
                    shutdown -r +15   15分钟后重启        
            - 查看当前系统时间:date
            - 查看日历:cal
            - 查看某年日历:cal 年份
            - 查看某年每月日历:cal 月份 年份
            - 查看系统当前IP地址:
                              
                              ifconfig
                              ip addr show
            - 测试网络连通性:
                          
                          ping 127.0.0.1
                          ping 127.0.0.1 -c 4 限制ping包次数
            - 
            - 准确的shell命令规范
                - 命令字 [选项] [参数]
                
        - BASH常用快捷方式:
            - CTRL + c: 终止当前操作
            - CTRL + l: = clear  清屏
            - "\": 换行
            - CTRL + a:光标切换到行首
            - CTRL + e:光标切换到行尾
            - CTRL + u:清空光标所在位置前面的所有内容
            - CTRL + k:清空光标所在位置后面的所有内容
            - history: 查看历史命令,默认保留1000行
            - !n: 调用历史命令
            - history -c:清除历史命令
            
        
        
        
        
    
