# 装饰器应用场景-统计函数执行时间

import time

# 定义一个统计函数执行时间的 装饰器
def runtime(f):
    def inner():
        start = time.perf_counter()
        f()
        end =  time.perf_counter() - start
        print(f'函数的调用执行时间为：{end}')
    return inner

# 定义一个函数
@runtime
def func():
    for i in range(5):
        print(i,end=" ")
        time.sleep(1)

func()




