# 递归函数的练习

# 斐波那契数列 1,1,2,3,5,8,13
num = 0
def feibo(n):
    global num
    num += 1
    if n == 1 or n == 2:
        return 1
    else:
        return feibo(n-1) + feibo(n-2)

res = feibo(6)
print(res)
print(num)

'''
feibo(6) ==> feibo(5) + feibo(4)
            ==> feibo(4) + feibo(3)
                ==> feibo(3) + feibo(2)
                    ==> feibo(2) + feibo(1)
'''